<?php

namespace Youyan\Generate\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Youyan\Generate\Code;

class GenerateCodeCommand extends Command
{
    protected Code $code;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:auto {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws BindingResolutionException
     */
    public function handle()
    {
        $table = $this->argument('table');
        if(empty($table))
            $this->error("请输入表名");
        /** @var Code $code */
        $code = app()->make(Code::class, ['table' => $table]);
        $this->generateFileRequest($code);
        return 1;
    }

    private function generateFileRequest(Code $code) {
        $requests = [
            'Controller' => '控制器',
            'Service' => '服务层',
            'ServiceImpl' => '服务实现层',
            'Model' => '模型层',
            'SelectListRequest' => '查询列表数据请求',
            'SelectRequest' => '查询单数据请求',
            'CreateRequest' => '创建请求',
            'UpdateRequest' => '更新请求',
            'DeleteRequest' => '删除请求',
        ];

        foreach ($requests as $method => $request) {
            $codeFullMethod = 'generate' . $method . 'File';
            if($code->{$codeFullMethod}())
                $this->info("生成{$request}文件成功");
            else
                $this->error("生成{$request}文件失败");
        }
    }
}
