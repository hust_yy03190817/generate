<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\RequestCodeBlockGenerate;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class RequestService
{
    const MESSAGES = [
        'required' => ":attribute为必填参数",
        'string' => ":attribute必须为字符串",
        'min' => ":attribute最少:size个字符",
        'max' => ":attribute最多:size个字符",
        'integer' => ":attribute必须为整数",
        'nullable' => ":attribute可以为null"
    ];
}
