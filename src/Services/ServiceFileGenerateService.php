<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class ServiceFileGenerateService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $templatePath;

    protected string $servicePath;

    public function __construct($tableName, $tableChineseName, $templatePath, $servicePath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->templatePath = $templatePath;
        $this->servicePath = $servicePath;
    }

    public function generate(): bool
    {
        $dist = $this->servicePath . ucfirst(StringUtils::camelize($this->tableName)) . 'Service.php';
        if(is_file($dist))
            unlink($dist);
        $serviceImplTemplate = file_get_contents( $this->templatePath . 'service.template');
        return $this->createTemplate($serviceImplTemplate, $this->tableName, $this->tableChineseName, "", "", $dist);
    }
}
