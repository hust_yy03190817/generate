<?php

namespace Youyan\Generate\Traits;

trait RequestCodeBlockGenerate
{
    private function makeFiledRule(string $fieldName, string $fieldType, string|null $default, string $isNullable) {
        $str = "\t\t\t'{$fieldName}' => ";
        if(str_contains($fieldType, 'int')) {
            if(str_contains($fieldType, 'unsigned')) {
                $str .= " 'integer|min:1'";
            } else {
                $str .= " 'integer'";
            }
        } else if(str_contains($fieldType, 'varchar') || str_contains($fieldType, 'text')) {
            $str .= " 'string'";
        }
        if(is_null($default)) {
            $str = rtrim($str, "'") . "|required'";
        }
        if($isNullable == 'YES') {
            $str = rtrim($str, "'") . "|nullable'";
        }
        return $str.",\n";
    }

    private function makeFileMessages(string $fieldName, string $fieldType, string|null $default, string $isNullable) {
        $str = "";
        if(str_contains($fieldType, 'int')) {
            if(str_contains($fieldType, 'unsigned')) {
                $str .= "\t\t\t'{$fieldName}:integer' => '" . self::MESSAGES['integer'] . "',\n";
                $str .= "\t\t\t'{$fieldName}:min' => '" . self::MESSAGES['min'] . "',\n" ;
            } else {
                $str .= "\t\t\t'{$fieldName}:integer' => '" . self::MESSAGES['integer'] . "',\n";
            }
        } else if(str_contains($fieldType, 'varchar')) {
            $str .= "\t\t\t'{$fieldName}:string' => '" . self::MESSAGES['string'] . "',\n";
        }
        if(is_null($default)) {
            $str .= "\t\t\t'{$fieldName}:required' => '" . self::MESSAGES['required'] . "',\n";
        }
        if($isNullable == 'YES') {
            $str .= "\t\t\t'{$fieldName}:nullable' => '" . self::MESSAGES['nullable'] . "',\n";
        }
        return $str;
    }
}
