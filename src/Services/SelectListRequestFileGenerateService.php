<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class SelectListRequestFileGenerateService extends RequestService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $firstNonPriColumnName;

    protected string $templatePath;

    protected string $requestPath;

    public function __construct($tableName, $tableChineseName, $firstNonPriColumnName, $templatePath, $requestPath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->firstNonPriColumnName = $firstNonPriColumnName;
        $this->templatePath = $templatePath;
        $this->requestPath = $requestPath;
    }

    public function generate(): bool
    {
        $dist = $this->requestPath . 'Select' . ucfirst(StringUtils::camelize($this->tableName)) . 'ListRequest.php';
        if(is_file($dist))
            unlink($dist);
        $selectListRequestTemplate = file_get_contents( $this->templatePath . 'requests' . DIRECTORY_SEPARATOR . 'selectListRequest.template');
        return $this->createTemplate($selectListRequestTemplate, $this->tableName, $this->tableChineseName, $this->firstNonPriColumnName, "", $dist);
    }
}
