<?php

namespace Youyan\Generate\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Container\BindingResolutionException;
use Youyan\Generate\Code;

class InitializeCodeCommand extends Command
{
    protected Code $code;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'code:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '生成BusinessException异常类, Handler异常处理类, CommonRequest请求基类';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws BindingResolutionException
     */
    public function handle(): int
    {
        /** @var Code $code */
        $code = app()->make(Code::class);
        $this->generateInitializeFileRequest($code);
        return 1;
    }

    private function generateInitializeFileRequest(Code $code) {
        $method = 'generateInitializeFile';
        if(!$code->{$method}())
            $this->error("生成初始化文件失败");
        else
            $this->info("生成初始化文件成功");
    }
}
