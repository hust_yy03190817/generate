<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class ModelFileGenerateService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $fillColumns;

    protected string $templatePath;

    protected string $modelPath;

    public function __construct($tableName, $tableChineseName, $fillColumns, $templatePath, $modelPath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->fillColumns = $fillColumns;
        $this->templatePath = $templatePath;
        $this->modelPath = $modelPath;
    }

    public function generate(): bool
    {
        $dist = $this->modelPath . ucfirst(StringUtils::camelize($this->tableName)) . '.php';
        if(is_file($dist))
            unlink($dist);
        $modelTemplate = file_get_contents( $this->templatePath . 'model.template');
        return $this->createTemplate($modelTemplate, $this->tableName, $this->tableChineseName, "", $this->fillColumns, $dist);
    }
}
