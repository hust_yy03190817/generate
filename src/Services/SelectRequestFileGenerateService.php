<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class SelectRequestFileGenerateService extends RequestService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $templatePath;

    protected string $requestPath;

    public function __construct($tableName, $tableChineseName, $templatePath, $requestPath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->templatePath = $templatePath;
        $this->requestPath = $requestPath;
    }

    public function generate(): bool
    {
        $dist = $this->requestPath . 'Select' . ucfirst(StringUtils::camelize($this->tableName)) . 'Request.php';
        if(is_file($dist))
            unlink($dist);
        $selectRequestTemplate = file_get_contents( $this->templatePath . 'requests' . DIRECTORY_SEPARATOR . 'selectRequest.template');
        return $this->createTemplate($selectRequestTemplate, $this->tableName, $this->tableChineseName, "", "", $dist);
    }
}
