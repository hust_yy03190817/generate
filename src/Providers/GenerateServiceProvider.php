<?php

namespace Youyan\Generate\Providers;

use Youyan\Generate\Console\Commands\GenerateCodeCommand;
use Illuminate\Support\ServiceProvider;
use Youyan\Generate\Code;
use Youyan\Generate\Console\Commands\InitializeCodeCommand;

class GenerateServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton('auto-code', function($app) {
            return $app->make(Code::class);
        });
    }

    public function boot() {
        $this->publishes([
            __DIR__ . '../../config/code.php' => config_path('code.php'),
        ]);

        if ($this->app->runningInConsole()) {
            $this->commands([
                GenerateCodeCommand::class,
                InitializeCodeCommand::class
            ]);
        }
    }
}
