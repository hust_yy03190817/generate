<?php

namespace Youyan\Generate;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Support\Facades\DB;
use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Core\Generate;
use Youyan\Generate\Services\ControllerFileGenerateService;
use Youyan\Generate\Services\CreateRequestFileGenerateService;
use Youyan\Generate\Services\DeleteRequestFileGenerateService;
use Youyan\Generate\Services\InitializeFileGenerateService;
use Youyan\Generate\Services\ModelFileGenerateService;
use Youyan\Generate\Services\SelectListRequestFileGenerateService;
use Youyan\Generate\Services\SelectRequestFileGenerateService;
use Youyan\Generate\Services\ServiceFileGenerateService;
use Youyan\Generate\Services\ServiceImplFileGenerateService;
use Youyan\Generate\Services\UpdateRequestFileGenerateService;

class Code
{
    private Generate $core;

    protected string $table;
    protected string $chineseTableName;

    protected FileGenerateContract $controllerFileGenerateService;
    protected FileGenerateContract $serviceFileGenerateService;
    protected FileGenerateContract $serviceImplFileGenerateService;
    protected FileGenerateContract $modelFileGenerateService;
    protected FileGenerateContract $createRequestFileGenerateService;
    protected FileGenerateContract $updateRequestFileGenerateService;
    protected FileGenerateContract $selectListRequestFileGenerateService;
    protected FileGenerateContract $selectRequestFileGenerateService;
    protected FileGenerateContract $deleteRequestFileGenerateService;

    protected FileGenerateContract $initializeFileGenerateService;

    /**
     * @throws BindingResolutionException
     */
    public function __construct(string|null $table = null)
    {
        $this->core = new Generate();
        if(is_null($table)) {
            $this->initializeFileGenerateService = app()->make(InitializeFileGenerateService::class, [
                'templatePath' => $this->config('paths.template_path'),
                'distPath' => $this->config('paths.initialize_path')
            ]);
        } else {
            $this->table = $table;
            $this->descTable();
        }
    }

    public function __call($method, $params) {
        $matches = [];
        preg_match('/^generate(.*?)File$/', $method, $matches);
        if(empty($matches))
            return false;

        $serviceName = lcfirst($matches[1]) . 'FileGenerateService';
        /** @var FileGenerateContract $service */
        $service = $this->{$serviceName};
        return $service->generate();
    }

    public function config(string $keys = '')
    {
        if(empty($keys))
            return $this->core->getConfig();
        $keys = explode('.', $keys);
        $result = $this->core->getConfig();
        foreach ($keys as $key) {
            $result = $result[$key];
        }
        return $result;
    }

    /**
     * @throws BindingResolutionException
     */
    public function descTable(): void
    {
        // 查询数据表结构
        $rows = DB::select("DESC " . $this->table);
        $collection = collect($rows);

        $fields = $collection->filter(function($value){
            return !in_array($value->Field, ['id', 'created_at', 'updated_at']);
        })->pluck('Field');

        $firstColumnName = $fields->toArray()[0];

        $fillColumns = implode(', ', $fields->map(function($val){
            return "'" . $val . "'";
        })->toArray());

        $this->chineseTableName = $this->table;

        $this->controllerFileGenerateService = app()->make(ControllerFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'firstNonPriColumnName' => $firstColumnName,
            'templatePath' => $this->config('paths.template_path'),
            'controllerPath' => $this->config('paths.controller_path'),
        ]);

        $this->serviceFileGenerateService = app()->make(ServiceFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'servicePath' => $this->config('paths.service_path'),
        ]);

        $this->serviceImplFileGenerateService = app()->make(ServiceImplFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'serviceImplPath' => $this->config('paths.service_impl_path'),
        ]);

        $this->modelFileGenerateService = app()->make(ModelFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'fillColumns' => $fillColumns,
            'templatePath' => $this->config('paths.template_path'),
            'modelPath' => $this->config('paths.model_path'),
        ]);

        $this->createRequestFileGenerateService = app()->make(CreateRequestFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'requestPath' => $this->config('paths.request_path'),
            'fields' => $rows
        ]);

        $this->updateRequestFileGenerateService = app()->make(UpdateRequestFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'requestPath' => $this->config('paths.request_path'),
            'fields' => $rows
        ]);

        $this->selectListRequestFileGenerateService = app()->make(SelectListRequestFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'firstNonPriColumnName' => $firstColumnName,
            'templatePath' => $this->config('paths.template_path'),
            'requestPath' => $this->config('paths.request_path')
        ]);

        $this->selectRequestFileGenerateService = app()->make(SelectRequestFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'requestPath' => $this->config('paths.request_path')
        ]);

        $this->deleteRequestFileGenerateService = app()->make(DeleteRequestFileGenerateService::class, [
            'tableName' => $this->table,
            'tableChineseName' => $this->chineseTableName,
            'templatePath' => $this->config('paths.template_path'),
            'requestPath' => $this->config('paths.request_path')
        ]);
    }
}
