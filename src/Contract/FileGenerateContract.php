<?php

namespace Youyan\Generate\Contract;

interface FileGenerateContract
{
    public function generate();
}
