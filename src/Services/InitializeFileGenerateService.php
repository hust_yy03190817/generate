<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\RequestCodeBlockGenerate;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class InitializeFileGenerateService extends RequestService implements FileGenerateContract
{
    protected string $templatePath;

    protected string $distPath;

    public function __construct(string $templatePath, string $distPath)
    {
        $this->distPath = $distPath;
        $this->templatePath = $templatePath;
    }

    public function generate(): bool
    {
        // 生成也无异常文件
        $dist = $this->distPath . 'BusinessException.php';
        $businessExceptionTemplate = file_get_contents($this->templatePath . 'businessException.template');
        if(!file_put_contents($dist, $businessExceptionTemplate))
            return false;

        // 生成默认状态码文件
        $dist = $this->distPath . 'HttpStatusCode.php';
        $httpStatusCodeTemplate = file_get_contents($this->templatePath . 'httpStatusCode.template');
        if(!file_put_contents($dist, $httpStatusCodeTemplate))
            return false;

        // 生成异常处理文件
        $dist = $this->distPath . 'Handler.php';
        $handlerTemplate = file_get_contents($this->templatePath . 'exceptionHandler.template');
        if(!file_put_contents($dist, $handlerTemplate))
            return false;

        // 生成请求基类文件
        $dist = $this->distPath . 'CommonRequest.php';
        $commonRequestTemplate = file_get_contents($this->templatePath . 'requests' . DIRECTORY_SEPARATOR . 'commonRequest.template');
        if(!file_put_contents($dist, $commonRequestTemplate))
            return false;

        return true;
    }
}
