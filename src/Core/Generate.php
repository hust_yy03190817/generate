<?php

namespace Youyan\Generate\Core;

class Generate
{
    protected array $config = [];

    public function __construct()
    {
        $this->initConfig();
        $this->initDir();
    }

    private function initConfig(): void
    {
        if(!is_file(config_path('code.php')))
            $this->config = require __DIR__ . '/../../config/code.php';
        else
            $this->config = require config_path('code.php');
    }

    public function initDir(): void
    {
        $paths = $this->getConfig()['paths'];
        foreach ($paths as $path) {
            if(!is_dir($path))
                mkdir($path);
        }
    }

    public function getConfig(): array
    {
        return $this->config;
    }
}
