<?php

namespace Youyan\Generate\Traits;

use Youyan\Generate\Utils\StringUtils;

trait TemplateStringReplace
{
    public function createTemplate(string $template, string $tableName, string $tableChineseName, string $firstColumnName, string $fillColumns, string $dist): bool|int
    {
        $tableChineseName = $tableChineseName ?: $tableName;
        $template = str_replace('{$tableExplain^}', $tableChineseName, $template);
        $template = str_replace('{$ucfirstTableName^}', ucfirst(StringUtils::camelize($tableName)), $template);
        $template = str_replace('{$tableName^}', StringUtils::camelize($tableName), $template);
        $template = str_replace('{$snakeTableName^}', StringUtils::uncamelize($tableName), $template);
        $template = str_replace('{$ucTableName^}', strtoupper($tableName), $template);
        $template = str_replace('{$firstColumnName^}', $firstColumnName, $template);
        $template = str_replace('{$tableFillColumns^}', $fillColumns, $template);
        return file_put_contents($dist, $template);
    }
}
