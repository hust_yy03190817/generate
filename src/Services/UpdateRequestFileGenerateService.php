<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\RequestCodeBlockGenerate;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class UpdateRequestFileGenerateService extends RequestService implements FileGenerateContract
{
    use TemplateStringReplace, RequestCodeBlockGenerate;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $templatePath;

    protected string $requestPath;

    protected mixed $fields;

    public function __construct($tableName, $tableChineseName, $templatePath, $requestPath, $fields)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->templatePath = $templatePath;
        $this->requestPath = $requestPath;
        $this->fields = $fields;
    }

    public function generate(): bool
    {
        $dist = $this->requestPath . 'Update' . ucfirst(StringUtils::camelize($this->tableName)) . 'Request.php';
        if(is_file($dist))
            unlink($dist);
        $createRequestTemplate = file_get_contents( $this->templatePath . 'requests' . DIRECTORY_SEPARATOR . 'updateRequest.template');
        $rules = "";
        $messages = "";
        foreach ($this->fields as $field) {
            if($field->Key == 'PRI' || in_array($field->Field, ['created_at', 'updated_at']))
                continue;
            $rules .= $this->makeFiledRule($field->Field, $field->Type, $field->Default, $field->Null);
            $messages .= $this->makeFileMessages($field->Field, $field->Type, $field->Default, $field->Null);
        }
        $createRequestTemplate = str_replace('{$rules^}', rtrim($rules, ",\n"), $createRequestTemplate);
        $createRequestTemplate = str_replace('{$messages^}', rtrim($messages, ",\n"), $createRequestTemplate);

        return $this->createTemplate($createRequestTemplate, $this->tableName, $this->tableChineseName, "", "", $dist);
    }
}
