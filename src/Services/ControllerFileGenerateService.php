<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class ControllerFileGenerateService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $firstNonPriColumnName;

    protected string $templatePath;

    protected string $controllerPath;

    public function __construct($tableName, $tableChineseName, $firstNonPriColumnName, $templatePath, $controllerPath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->firstNonPriColumnName = $firstNonPriColumnName;
        $this->templatePath = $templatePath;
        $this->controllerPath = $controllerPath;
    }

    public function generate(): bool
    {
        $dist = $this->controllerPath . ucfirst(StringUtils::camelize($this->tableName)) . 'Controller.php';
        if(is_file($dist))
            unlink($dist);
        $controllerTemplate = file_get_contents( $this->templatePath . 'controller.template');
        return $this->createTemplate($controllerTemplate, $this->tableName, $this->tableChineseName, $this->firstNonPriColumnName, "", $dist);
    }
}
