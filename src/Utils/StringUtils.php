<?php

namespace Youyan\Generate\Utils;

class StringUtils
{
    /**
     * 下划线转驼峰
     * @param string $uncamelized_words
     * @param string $separator
     * @return string
     */
    public static function camelize(string $uncamelized_words, string $separator='_'): string
    {
        $uncamelized_words = $separator. str_replace($separator, " ", strtolower($uncamelized_words));
        return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator );
    }

    /**
     * 驼峰命名转下划线命名
     * @param string $camelCaps
     * @param string $separator
     * @return string
     */
    public static function uncamelize(string $camelCaps, string $separator='_'): string
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
    }
}
