<?php

return [
    'module' => 'Admin',

    'paths' => [
        'template_path' => __DIR__ . '/../src/Templates/',
        'controller_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR),
        'service_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR),
        'service_impl_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR),
        'model_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR),
        'request_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'request' . DIRECTORY_SEPARATOR),
        'initialize_path' => storage_path('app' . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR),
    ]
];
