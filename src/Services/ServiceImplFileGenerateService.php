<?php

namespace Youyan\Generate\Services;

use Youyan\Generate\Contract\FileGenerateContract;
use Youyan\Generate\Traits\TemplateStringReplace;
use Youyan\Generate\Utils\StringUtils;

class ServiceImplFileGenerateService implements FileGenerateContract
{
    use TemplateStringReplace;

    protected string $tableName;

    protected string $tableChineseName;

    protected string $templatePath;

    protected string $serviceImplPath;

    public function __construct($tableName, $tableChineseName, $templatePath, $serviceImplPath)
    {
        $this->tableName = $tableName;
        $this->tableChineseName = $tableChineseName;
        $this->templatePath = $templatePath;
        $this->serviceImplPath = $serviceImplPath;
    }

    public function generate(): bool
    {
        $dist = $this->serviceImplPath . ucfirst(StringUtils::camelize($this->tableName)) . 'ServiceImpl.php';
        if(is_file($dist))
            unlink($dist);
        $serviceImplTemplate = file_get_contents( $this->templatePath . 'serviceImpl.template');
        return $this->createTemplate($serviceImplTemplate, $this->tableName, $this->tableChineseName, "", "", $dist);
    }
}
